class Character {
  name = "";
  role = "";
  constructor(name, role) {
    this.name = name;
    this.role = role;
  }
  characterArmor(armor) {
    console.log(`${this.name} is ${this.role} and have ${armor} armor`);
  }

  characterDemage(damage) {
    console.log(`${this.name} is ${this.role} and have ${damage} attak damage`);
  }
}

class Mage extends Character {
  constructor(name, role) {
    super(name, role);
  }

  characterDemage(damage) {
    console.log(
      `${this.name} is ${this.role} and have ${damage} Magic Damage `
    );
  }
  magicPowers() {
    console.log(`${this.name} can fly`);
  }
}

class Support extends Mage {
  heal = 0;
  constructor(name, role, heal) {
    super(name, role);
    this.heal = heal;
  }
  characterDemage(damage) {
    console.log(
      `${this.name} is ${this.role} and have  ${damage} Magic Damage `
    );
  }
  support() {
    console.log(`${this.name} can heal you ${this.heal} `);
  }
}

class Adc extends Character {
  constructor(name) {
    super(name, "adc");
  }

  range(num) {
    if (num < 30) {
      console.log(`${this.name} is in close range`);
    } else if (num > 30) {
      console.log(`${this.name} is in far range`);
    }
  }
}

let yasuo = new Character("yasuo", "Assasin");
console.log(yasuo);
let diana = new Mage("diana", "Mage");
let sonna = new Support("sonna", "Mage", 40);
let vayne = new Adc("vayne");

yasuo.characterDemage(13);
diana.characterDemage(23);
sonna.characterDemage(31);

sonna.support();
console.log(vayne);
vayne.range(2);
